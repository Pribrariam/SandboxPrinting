# SandboxPrinting

A talk given at NYLA Annual Conference 2022.

[Slides here.](https://pribrariam.codeberg.page/sandboxprinting/)

## Components of the project:

### Printing.wsb

A configuration file for Windows Sandbox written in XML. It is also used as an icon on the desktop to launch the sandbox.

- [Windows Sandbox - Microsoft Docs](https://docs.microsoft.com/en-us/windows/security/threat-protection/windows-sandbox/windows-sandbox-overview)

### RemotePrintCleanup.js

A Google Apps Script to delete all emails and Drive files. It is set to run every night at 1am.

In the file I have documented my process using comment blocks surrounded by `/*` and `*/`. I have used double backslashes (`//`) to comment out lines for functional reasons.

- [Google Apps Script - Google Developers](https://developers.google.com/apps-script/overview)
- [Gmail API - Google Developers](https://developers.google.com/gmail/api/reference/rest)
- [Drive API - Google Developers](https://developers.google.com/drive/api/v2/reference) _(Apps script uses v2)_

### RemotePrintingQR.jpg _(Optional)_

We have this image posted on the front reference desk and available as palm card for patrons to take home. The QR code links to `mailto:printing@thetroylibrary.org`, so it should automatically open the patron's email app and populate the `to:` field when they scan it.

### ContactForm7.html _(Optional)_

Contact Form 7 is a free Wordpress plugin that adds configurable contact forms to your site. The `.html` file in the repo is the form template copy-pasted from the plugin page. Newer versions allow for building the template with the block editor, so this may not be necessary anymore.

On [our site](https://www.thetroylibrary.org/?cat=46), we use it to provide a way for patrons to upload files from anywhere. It's also a more private option since it isn't tied to the patron's own email address.

- [Contact Form 7 Docs](https://contactform7.com/docs/)

### Microsoft Defender Application Guard _(Not currently implemented)_

Microsoft Edge running natively in an isolated Hyper-V container.

- [Microsoft Defender Application Guard - Microsoft Docs](https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-application-guard/md-app-guard-overview)
