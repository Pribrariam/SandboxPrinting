function autoDeletePrints() {

  const today = new Date().toLocaleDateString();

  const threads = GmailApp.search("label:all before:" + today);
  /*
   * https://developers.google.com/apps-script/reference/gmail/gmail-app#searchquery
   * Like using the Gmail search bar
   * I want to include label:all because I want to grab sent emails as well as any threads that for whatever reason are not in the inbox.
   * Returns an array of GmailThreads[] (class)
   * Classes are part of JavaScript. They are a way of building objects that contain data and including code to interact with those data.
   */

  if (threads.length > 0) {
    console.log('Found ' + threads.length + ' email(s) to delete.');
    /* This lets me check on the status from the Apps Script dashboard. */
    for (const thread of threads) {
      console.log('Deleting email from ' + thread.getLastMessageDate());
      /* Useful for debugging. */
      Gmail.Users.Threads.remove('me', thread.getId());
      /*
       * https://developers.google.com/gmail/api/reference/rest/v1/users.threads/delete
       * This completely erases the thread. No trash.
       * Important to note that this is not a part of Apps Script. It's using the Gmail API.
       * We have to provide authorization to the script in order to have it work.
       */
    }
  } else {
    console.log('No emails to delete.');
  }
}

function autoDeleteDrive() {

  /*
   * We're using the Drive API here, like we used Gmail above.
   * There's two versions though, v2 and v3, and Apps Script can only use v2, so we want to be sure we're looking at the correct reference.
   * https://developers.google.com/drive/api/v2/reference
   */

  const driveFiles = Drive.Files.list({
    "q": "starred = false"
    });

  /*
   * This script gets saved as a file in Drive, so we need a way not to delete it when we run the script.
   * So we star it and then set our query to only include unstarred files.
   */

  if (driveFiles.items.length > 0) {
    console.log('Found ' + driveFiles.items.length + ' file(s) to delete.');
    for (const driveItem of driveFiles.items) {
      if (driveItem.owners[0].isAuthenticatedUser == true) {
        //console.log('Deleting file id: ' + driveItem.id);
        Drive.Files.remove(driveItem.id);
        /*
         * Pretty much same as the Gmail function above, but for Drive (using the API, not Apps Script.)
         * Make sure the Drive API is enabled in Services section on left.
         * API calls return JSON object, not a Javascript class, so the way of interacting is different.
         *
         * Test if the account is the owner of the file and if so, delete the file.
         */
      } else {
        /* If the account is not the owner, we have more complicated work to do. */

        //console.log('Removing shared permissions from: ' + driveItem.id);
        const itemPers = Drive.Permissions.list(driveItem.id);
        /*
         * Since the file has been shared with the account, there should be associated permissions.
         * Here we gather the array of permissions attached to the file.
         */

        //var permissionIdFile = Drive.Permissions.get(driveItem.id, 'me');
        //var filePars = Drive.Parents.list(driveItem.id);

        let permissionIdNum = "";

        for (const permis of itemPers.items) {
          if (permis.emailAddress == "printing@thetroylibrary.org") {
            permissionIdNum = permis.id;
            Drive.Permissions.remove(driveItem.id, permissionIdNum);
            /*
             * Then we iterate over those permissions looking for our account.
             * If we find it, we remove the associated permissions.
             */
          }
        }
        if (permissionIdNum == "") {
          /*
           * Here's a problem: sometimes, despite having access to a file, the above function doesn't result in a permission for the file.
           * I think it happens when someone emails a document using the "Insert files using Drive" option in Gmail.
           * It might also be when someone sends a public file available to anyone with the link. (Which wouldn't assign a permission to the specific account.)
           * In this case, the file shows up in the "Shared with me" section of Drive, and can be "erased" by right-clicking and hitting "Remove."
           * However, I've not found a way to do this programmatically.
           */
          //var fileFile = DriveApp.getFileById(driveFiles.items[i].id);
          //var fileViewers = fileFile.getViewers();
          //var fileSharingPer = fileFile.getSharingPermission().toString();
          //var fileEditor = fileFile.getEditors();
          //var fileAccess = fileFile.getAccess('printing@thetroylibrary.org').toString();
          //driveFiles.items[i].userPermission.setRole("none");
          //DriveApp.getFileById(driveFiles.items[i].id).setSharing(DriveApp.Access.PRIVATE,DriveApp.Permission.NONE);
          //DriveApp.getFileById(driveFiles.items[i].id).revokePermissions(driveFiles.items[i].userPermission.id);
          console.log("this part is broken")
        }
        //Drive.Permissions.remove(driveFiles.items[i].id, permissionIdNum);
        //var parentId = Drive.About.get().rootFolderId;
        //var parentInfo = Drive.Parents.list(driveFiles.items[i].id);
        //console.log(i + ' | UserPer: ' + permissionId + ' | File :' + driveFiles.items[i].id + ' | FilePer: ' + permissionIdFile);
        //Drive.Permissions.remove(driveFiles.items[i].id, fileInfo.items[0].id);
        //var fileInfoAccess = DriveApp.getFileById(driveFiles.items[i].id).getSharingAccess().toString();
        //var fileInfoSharing = DriveApp.getFileById(driveFiles.items[i].id).getSharingPermission().toString();
        //driveFiles.items[i].setShared(false);
        //removePermission(driveFiles.items[i].id, "me");
        //Drive.Permissions.remove(driveFiles.items[i].id, "me");
      };
      //console.log(i + ': ' + driveFiles.items[i].owners[0].isAuthenticatedUser + ' : ' + driveFiles.items[i].id);
    }
  } else {
    console.log('No files to delete.');
  }

//  var today = new Date().toISOString();
//  console.log(today);
//  var person0 = Session.getActiveUser().getUserLoginId();
//  console.log('Person: ' + person0);
//  var permissionId = Drive.About.get().permissionId;
//  console.log('Permission Id: ' + permissionId);
//
//  function removePermission (fileParam, permissionParam) {
//    var request = Drive.Permissions.remove({
//      'fileId': fileParam,
//      'permissionId': permissionParam
//    });
//    request.execute(function(resp) {});
//  };

}
